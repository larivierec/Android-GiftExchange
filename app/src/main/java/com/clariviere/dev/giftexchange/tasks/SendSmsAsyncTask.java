package com.clariviere.dev.giftexchange.tasks;

import android.os.AsyncTask;
import android.telephony.SmsManager;

import com.clariviere.dev.giftexchange.model.Person;

public class SendSmsAsyncTask extends AsyncTask<Void, Void, Void> {

    private SmsManager   mSmsManager;
    private Person       mPerson;

    public SendSmsAsyncTask(SmsManager manager, Person e){
        this.mSmsManager = manager;
        this.mPerson = e;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String message = "You have chosen ".concat(mPerson.getPersonPicked().getPersonName());
        mSmsManager.sendTextMessage(mPerson.getPhoneNumber(), null, message, null, null);
        return null;
    }
}
