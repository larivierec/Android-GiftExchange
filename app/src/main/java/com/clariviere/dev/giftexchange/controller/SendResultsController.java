package com.clariviere.dev.giftexchange.controller;


import android.telephony.SmsManager;
import android.view.View;
import android.widget.Toast;

import com.clariviere.dev.giftexchange.activities.MainActivity;
import com.clariviere.dev.giftexchange.model.Person;
import com.clariviere.dev.giftexchange.tasks.SendEmailToUserAsyncTask;
import com.clariviere.dev.giftexchange.tasks.SendSmsAsyncTask;
import com.google.api.services.gmail.Gmail;
import com.google.common.base.Strings;

import java.util.List;

public class SendResultsController implements View.OnClickListener{

    private MainActivity mActivity;
    private List<Person> mPeopleList;
    private Gmail        mService;
    private SmsManager   mSmsManager;

    public SendResultsController(MainActivity activity, List<Person> peopleList, Gmail service){
        this.mActivity = activity;
        this.mPeopleList = peopleList;
        this.mService = service;
        mSmsManager = SmsManager.getDefault();
    }

    @Override
    public void onClick(View v) {
        for(Person e : mPeopleList){
            if(!Strings.isNullOrEmpty(e.getPersonPicked().getEmail())) {
                new SendEmailToUserAsyncTask(mActivity, mService, e).execute();
            }
            else if(!Strings.isNullOrEmpty(e.getPersonPicked().getPhoneNumber())) {
                new SendSmsAsyncTask(mActivity, mSmsManager, e).execute();
            }
        }
        Toast.makeText(mActivity, "Emails have been sent.", Toast.LENGTH_SHORT).show();
    }
}
