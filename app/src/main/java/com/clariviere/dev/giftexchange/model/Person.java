package com.clariviere.dev.giftexchange.model;

import java.io.Serializable;

public class Person implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private int id;
	private String personName;
	private String email;
	private String phoneNumber;

	private Person personPicked;

    public Person(String name, String email, String phoneNumber){
        this.personName = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }
	
	public void setPersonPicked(Person p){
		this.personPicked = p;
	}
	
	public Person getPersonPicked(){
		return this.personPicked;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}
	
	public String getPersonName(){
		return this.personName;
	}

	public void setPersonEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return this.email;
	}

	public void setPersonID(int personID){
		this.id = personID;
	}

	public Integer getPersonID(){
		return this.id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
